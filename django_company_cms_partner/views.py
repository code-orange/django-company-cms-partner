from django.http import HttpResponse
from django.template import loader
from django.utils.translation import gettext as _


def home(request):
    template = loader.get_template("django_company_cms_partner/home.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Partner")
    template_opts["content_title_sub"] = _("Partner")

    return HttpResponse(template.render(template_opts, request))
