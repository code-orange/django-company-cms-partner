from django.contrib.sitemaps import Sitemap
from django.urls import reverse


class PartnerStaticViewSitemap(Sitemap):
    def items(self):
        return ["partner"]

    def location(self, item):
        return reverse(item)
